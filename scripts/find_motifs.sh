#!/bin/bash -l

usage()
{
cat<<EOF
  Usage: $0 [options] 
    <-s "summits" fasta [file]>
    <-p "peaks" fasta [file]>
    <-b meme background model [file]>
    <-r genome sequence fasta [file]>
    <-n basename for naming output file(s) [string]>
    <-o outdir [dir] (optional)>
EOF
exit 1
}

while getopts 's:p:b:r:n:o:' OPTION
do
  case $OPTION in
  s)    summit_seqs="$OPTARG"
        ;;
  p)    peak_seqs="$OPTARG"
        ;;
  b)    bgmodel="$OPTARG"
        ;;
  r)    ref_fasta="$OPTARG"
        ;;
  n)    basename="$OPTARG"
        ;;
  o)	outdir="$OPTARG"
	;;
  ?)    echo usage
        exit 1
        ;;
  esac
done

if [[ ! $summit_seqs ]] || \
   [[ ! $peak_seqs ]] || \
   [[ ! $bgmodel ]] || \
   [[ ! $basename ]] || \
   [[ ! $ref_fasta ]]; then
    echo "missing some arguments"
    usage
fi
if [[ ! -e $summit_seqs ]] || \
   [[ ! -e $peak_seqs ]] || \
   [[ ! -e $bgmodel ]] || \
   [[ ! -e $ref_fasta ]]; then
    echo "One or more arguments didn't exist"
    exit 1
fi
outdir=${outdir:="."}

# use meme suite to call 4 motifs: 2 for summits and 2 for peaks
# once with forced palindromic and once in standard mode
meme ${summit_seqs} -oc "${outdir}/${basename}_meme_summits" -bfile "${bgmodel}" -dna -revcomp -mod anr -nmotifs 1 -minw 8 -maxw 32
meme ${summit_seqs} -oc "${outdir}/${basename}_meme_summits_pal" -bfile "${bgmodel}" -dna -revcomp -mod anr -nmotifs 1 -minw 8 -maxw 32 -pal
meme ${peak_seqs} -oc "${outdir}/${basename}_meme_peaks" -bfile "${bgmodel}" -dna -revcomp -mod anr -nmotifs 1 -minw 8 -maxw 32
meme ${peak_seqs} -oc "${outdir}/${basename}_meme_peaks_pal" -bfile "${bgmodel}" -dna -revcomp -mod anr -nmotifs 1 -minw 8 -maxw 32 -pal

# fimo to map called motifs back to peaks
fimo -oc "${outdir}/${basename}_meme_summits" --parse-genomic-coord --qv-thresh --thresh 1e-2 "${outdir}/${basename}_meme_summits/meme.xml" "${peak_seqs}"
fimo -oc "${outdir}/${basename}_meme_summits_pal" --parse-genomic-coord --qv-thresh --thresh 1e-2 "${outdir}/${basename}_meme_summits_pal/meme.xml" "${peak_seqs}"
fimo -oc "${outdir}/${basename}_meme_peaks" --parse-genomic-coord --qv-thresh --thresh 1e-2 "${outdir}/${basename}_meme_peaks/meme.xml" "${peak_seqs}"
fimo -oc "${outdir}/${basename}_meme_peaks_pal" --parse-genomic-coord --thresh 1e-2 "${outdir}/${basename}_meme_peaks_pal/meme.xml" "${peak_seqs}"

echo "Called motifs:"
date +%T
