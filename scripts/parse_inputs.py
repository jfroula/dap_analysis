#!/usr/bin/env python

import argparse
import json
import pandas as pd
import subprocess
import os

### Parse arguments ###

parser = argparse.ArgumentParser()
parser.add_argument("-outfile", type=str, 
                    help="File name path/prefix for .json output",
                    default="./inputs")
parser.add_argument("-genomedir", type=str,
                    help="Directory with genome files",
                    default="/global/dna/projectdirs/RD/DAPseq/cromwell_genomes/")
parser.add_argument("-adapters", type=str,
                    help="Path to adapters fasta file for trimming",
                    default="/global/projectb/sandbox/gaag/bbtools/data/adapters.fa")
parser.add_argument("-samptable", type=str, required=True,
                    help="Path to table with library data (tsv). Must specify at least these columns: [\"library_name\",\"organism\", \"sample_type\", \"sample_name\"]")
parser.add_argument("-pipeout", type=str,
                    help="Directory where output files are copied",
                    default="/global/projectb/scratch/leo/pipeout/")
parser.add_argument("-fastqdir", type=str,
                    help="Path to dir with manually demultiplexed fastq.gz files")
parser.add_argument("-amplified", type=str,
                    help="Was input library amplified? (string)")
parser.add_argument("-maxfrags", type=int,
                    help="Max number of fragments to use for alignment (int)")
parser.add_argument('-nomotifs', action='store_true',
                    help="Set to False to skip motif calling (bool)")

args = parser.parse_args()

outfile = args.outfile
genomedir = args.genomedir
adapters = args.adapters
samptable = args.samptable
pipeout = args.pipeout
fastqdir = args.fastqdir
amplified = args.amplified
maxfrags = args.maxfrags
nomotifs = args.nomotifs

### Function definitions ###

def get_jamo_info(library):
    p = subprocess.run(['jamo', 'info', 'library', library], encoding='utf-8', stdout=subprocess.PIPE)
    jamo_info = p.stdout

    if jamo_info.strip() == "No matching records found":
        print("No JAMO record found for library: "+library)
    else:
        if len(jamo_info.split()) > 4:
            print("Found multiple entries for library {}. Using last entry:".format(library))
            print(jamo_info.strip()+'\n')
        [path, status] = jamo_info.split()[-3:-1]
        return pd.Series([library, path, status], index=['library_name', 'raw_fastq_path', 'status'])

def read_genome_size(gsizefile):
    with open(gsizefile) as f:
        return int(f.readline())


def fetch_purged(purged_libs):
    print("{} libraries found with status PURGED.".format(len(purged_libs)))

    if len(purged_libs) > 500:
        print("JAMO can't handle more than 500 libraries at once. Fetching multiple times...")

    for chunk in [purged_libs[i:i+500] for i in range(0, len(purged_libs), 500)]:
        print("Fetching {} libraries with command:".format(len(chunk)))
        fetch_command = ['jamo', 'fetch', '-w', 'library'] + chunk
        print(*fetch_command, sep=' ')
        print("Waiting...")
        subprocess.run(fetch_command, encoding='utf-8', stdout=subprocess.PIPE)

def add_not_in_jamo(samptable, fastqdir):
    # assume all libs in fastqdir are named starting with library name
    manual_libs = {}
    for fq in os.listdir(fastqdir):
        libname = fq.split('.')[0]
        manual_libs[libname] = fq

    for i in samptable.index:
        if samptable.loc[i,'library_name'] in manual_libs:
            samptable.loc[i,'raw_fastq_path'] = fastqdir + '/' + manual_libs[samptable.loc[i,'library_name']]
    return samptable

def write_inputs_json(samptable, outfile, adapters, genomedir, genome_name, gsize, pipeout):
    
    # split table between controls and experimental samples
    expt_samples = samptable[samptable['sample_type'] == 'expt']
    ctl_samples = samptable[samptable['sample_type'] == 'ctl']

    # make dict for output as JSON
    inputsJSON = {
        'jgi_dap_leo.adapters': adapters,
        'jgi_dap_leo.genome_fasta': genomedir+'/'+genome_name+'/'+genome_name+'.fasta',
        'jgi_dap_leo.bt2index_file1': genomedir+'/'+genome_name+'/'+genome_name+'_bt2index'+'/'+genome_name+'.1.bt2',
        'jgi_dap_leo.effgsize': gsize,
        'jgi_dap_leo.genes_gff': genomedir+'/'+genome_name+'/'+genome_name+'_filt.gff',
        'jgi_dap_leo.bgmodel': genomedir+'/'+genome_name+'/'+genome_name+'.bgmodel',
        'jgi_dap_leo.outdir': pipeout,
        'jgi_dap_leo.amplified': amplified,
        'jgi_dap_leo.maxfrags': maxfrags,
        'jgi_dap_leo.find_motifs': not nomotifs,
        'jgi_dap_leo.ctl_raw_fastqs': [],
        'jgi_dap_leo.expt_raw_fastqs': expt_samples['raw_fastq_path'].tolist(),
        'jgi_dap_leo.library_names_map': samptable.set_index('raw_fastq_path').to_dict()['library_name'],
        'jgi_dap_leo.sample_names_map': samptable.set_index('raw_fastq_path').to_dict()['sample_name']
    }

    # check if any controls, if True update inputsJSON dict
    if ctl_samples.shape[0] > 0:
        inputsJSON['jgi_dap_leo.ctl_raw_fastqs'] = ctl_samples['raw_fastq_path'].tolist()

    # write JSON to file
    with open(outfile, 'w') as outfile:
        json.dump(inputsJSON, outfile, indent=4)

### Read input table, make an inputs.JSON file for each organism ###
samptable = pd.read_csv(samptable, sep='\t')
for organism, subtable in samptable.groupby('input_organism'):
    subtable = subtable.copy()

    print("Processing sample table for organism: {}".format(organism))
    
    # read genome size from file
    try:
        gsizefile = genomedir+'/'+organism+'/'+organism+'.gsize'
        gsize = read_genome_size(gsizefile)
    except FileNotFoundError as e:
        print(e)
        print("Something is wrong with the gsize file: {}".format(gsizefile))
        exit(1)
    
    # get jamo fastq paths if they exist, and fetch purged files
    #jamo_info = subtable['library_name'].apply(get_jamo_info)
    #purged_libs = jamo_info[jamo_info['status'] == "PURGED"]
    #if purged_libs.shape[0] > 0:
    #    fetch_purged(purged_libs['library_name'].tolist())

    # add jamo fastq paths to df, overwrite with manual dir fastq if available
    #subtable = pd.concat((subtable, jamo_info['raw_fastq_path']), axis=1)
    #subtable['raw_fastq_path'] = ''
    subtable = add_not_in_jamo(samptable=subtable, fastqdir=fastqdir)

    # write final output file
    write_inputs_json(samptable=subtable,
                    outfile=outfile+'_'+organism+".json",
                    adapters=adapters,
                    genomedir=genomedir,
                    genome_name=organism,
                    gsize=gsize,
                    pipeout=pipeout)

