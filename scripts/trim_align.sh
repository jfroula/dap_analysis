#!/bin/bash -l

usage()
{
cat<<EOF
  Usage: $0 [options] 
    <-i input fastq [file]>
    <-n basename for naming output file(s) [string]>
    <-a illumina adapters fasta file [file]>
    <-e effective genome size (bp) [int]>
    <-r bowtie2 ref genome index [dir/basename]>
    <-s subsample to max this many fragments [int]>
    <-p threads [int]>
    <-m max memory in GB [int]>
    <-o outdir [dir] (optional)>
EOF
exit 1
}

while getopts 'i:n:a:e:r:s:p:m:o:' OPTION
do
  case $OPTION in
  i)    raw_fastq="$OPTARG"
        ;;
  n)    basename="$OPTARG"
        ;;
  a)    adapters="$OPTARG"
        ;;
  e)    effgsize="$OPTARG"
        ;;
  r)    bt2index="$OPTARG"
        ;;
  s)    maxfrags="$OPTARG"
        ;;
  p)	threads="$OPTARG"
        ;;
  m)    memory_gb="$OPTARG"
        ;;
  o)    outdir="$OPTARG"
        ;;
  ?)    echo usage
        exit 1
        ;;
  esac
done

if [[ ! $raw_fastq ]] || \
   [[ ! $basename ]] || \
   [[ ! $adapters ]] || \
   [[ ! $effgsize ]] || \
   [[ ! $bt2index ]] || \
   [[ ! $maxfrags ]] || \
   [[ ! $threads ]] || \
   [[ ! $memory_gb ]]; then
    echo "missing some arguments"
    usage
fi
if [[ ! -e $raw_fastq ]]; then
    echo "TRIM FQ FILE: $trimfqpath not found"
    exit 1
fi
if [[ ! -e $adapters ]]; then
    echo "ADAPTERS: $adapters not found"
    exit 1
fi
# check for existence of bt2 index?

outdir=${outdir:="."}

echo "Trimming + alignment start:"
date +%T

# give bbtools all mem except for 1GB
bbduk_xmx=$(($memory_gb - 1))

# only use up to maxfrags reads
reformat.sh \
in="${raw_fastq}" \
out="${basename}_sub.fastq" \
-Xmx${bbduk_xmx}g \
samplereadstarget=${maxfrags} \
verifyinterleaved=t

# BBTools to trim illumina adapters and quality filter
bbduk.sh \
-Xmx${bbduk_xmx}g \
threads=${threads} \
in=${basename}_sub.fastq \
out="${basename}_sub_trim.fastq" \
ref=${adapters} \
k=21 mink=11 ktrim=r tbo tpe qtrim=r trimq=6 maq=10 ow=t \
2> "${basename}_trim_stats.txt"

# Align to ref genome, filter for mapq 10, and convert sam to bam
bowtie2 \
--threads ${threads} \
-x "${bt2index}" \
--interleaved "${basename}_sub_trim.fastq" \
--no-mixed --no-discordant \
2> "${basename}_align_stats.txt" \
| samtools view -q 10 -b - \
| samtools sort -o "${outdir}/${basename}.bam" -


# Create index for .bam file
samtools index "${outdir}/${basename}.bam"

# Write percent aligned to file for stats
grep "% overall alignment rate" "${basename}_align_stats.txt" \
| awk -v FS="% " '{print $1}' > "${basename}_alignRate.txt"

# DeepTools to calculate median fragment size, write to file for stats
bamPEFragmentSize -b "${outdir}/${basename}.bam" \
| grep Median: \
| awk 'NR==1 {print$2}' \
> "${basename}_avgFragSize.txt"

# Make bigwig, normalizing to 1x coverage
bamCoverage \
-p ${threads} \
-b "${outdir}/${basename}.bam" \
-o "${outdir}/${basename}_dedup_norm.bw" \
--effectiveGenomeSize ${effgsize} \
--extendReads --binSize 1  \
--maxFragmentLength 600 --normalizeUsing RPGC

echo "Trimming + alignment finished:"
date +%T
