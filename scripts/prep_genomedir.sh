### simple wrapper script to run prep_genome on a directory of files
# note that files MUST be named with proper extensions .fna and .gff
# and basename must be the organism name and match between the files

genome_src=$1
outdir="/global/projectb/scratch/leo/genomes_prepped"

mkdir $outdir

for genome in ${genome_src}/*.gff; do

name=$(basename $genome .gff)
echo "preparing genome files for $name"

/global/projectb/scratch/leo/cromwell_dap/leo_dap/scripts/prep_genome.sh \
-b $name \
-f ${genome_src}/${name}.fna \
-g ${genome_src}/${name}.gff \
-o ${outdir}

done
