#!/bin/bash -l

usage()
{
cat<<EOF
  Usage: $0 [options] 
    <-b genome_name of genome [string]>
    <-f genome fasta [file]>
    <-g annotation gff [file]>
    <-o outdir for genome files [dir]>

EOF
exit 1
}

while getopts 'b:f:g:o:' OPTION
do
  case $OPTION in
  b)    genome_name="$OPTARG"
        ;;
  f)    fasta="$OPTARG"
        ;;
  g)    gff="$OPTARG"
        ;;
  o)    outdir="$OPTARG"
        ;;
  ?)    echo usage
        exit 1
        ;;
  esac
done

if [[ ! $genome_name ]] || \
   [[ ! $fasta ]] || \
   [[ ! $gff ]] || \
   [[ ! $outdir ]]; then
   echo "missing some arguments"
   usage
   exit 1
fi

if [[ ! -d ${outdir} ]]; then
   echo "Destination: ${outdir} not found."
   exit 1
fi

cd ${outdir}
mkdir ${genome_name}
cd ${genome_name}

cp ${fasta} "${genome_name}.fasta"
shifter --image=leobaumgart/dap_py3:2.1 \
samtools faidx "${genome_name}.fasta"

grep -v ">" "${genome_name}.fasta" | wc | awk '{print $3-$1}' > "${genome_name}.gsize"

shifter --image=leobaumgart/dap_py2:2.1 \
fasta-get-markov -m 0 -dna "${genome_name}.fasta" "${genome_name}.bgmodel"


cp ${gff} "./${genome_name}.gff"

shifter --image=leobaumgart/dap_py3:2.1 \
sort -k1,1 -k4n,4 ${gff} | \
uniq | \
awk -v FS='\t' '$3=="CDS" || $3=="tRNA" || $3=="rRNA" || $3=="pseudogene" || $3=="pseudogenic_tRNA" {print $0}' | \
sed -e 's/ /_/g' | \
sort -k1,1 -k4n,4 \
> "./${genome_name}_filt.gff"

mkdir "${genome_name}_bt2index"
cd "${genome_name}_bt2index"
shifter --image=leobaumgart/dap_py3:2.1 bowtie2-build -f ../${genome_name}.fasta ${genome_name}
